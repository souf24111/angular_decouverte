import { WrittingComponent } from './messaging/writting/writting.component';
import { UpdateMessageComponent } from "./messaging/update-message/update-message.component";import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CountComponent } from './messaging/count/count.component';
import { TchatComponent } from './messaging/tchat/tchat.component';


@NgModule({
  imports:[
    RouterModule.forRoot([
      {path:'tchat',component : TchatComponent },
      {path:'count',component : CountComponent },
      {path:'writting',component : WrittingComponent },
      {path: 'updatemsg/{{id}}', component : UpdateMessageComponent},
    ])
  ],
  exports : [
    RouterModule

  ]
})


export class RoutingModule {

}
