import { NgModule } from '@angular/core';
import { CommonModule, FormStyle } from '@angular/common';
import { TchatComponent } from './tchat/tchat.component';
import { CountComponent } from './count/count.component';
import { TchatMessageComponent } from './tchat-message/tchat-message.component';
import { WrittingComponent } from './writting/writting.component';
import { FormsModule } from '@angular/forms';
import { InboxComponent } from './inbox/inbox.component';
import { NotificationComponent } from './notification/notification.component';
import { UpdateMessageComponent } from './update-message/update-message.component';



@NgModule({
  declarations: [
    TchatComponent,
    CountComponent,
    TchatMessageComponent,
    WrittingComponent,
    InboxComponent,
    NotificationComponent,
    UpdateMessageComponent,

  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    TchatComponent,
    CountComponent,
    TchatMessageComponent
  ]
})
export class MessagingModule { }
