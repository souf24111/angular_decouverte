
import { Component, OnInit } from '@angular/core';
import { Message } from '../inbox/inbox.component';
import { DataMessagesService } from '../services/data-messages.service';


@Component({
  selector: 'app-writting',
  templateUrl: './writting.component.html',
  styleUrls: ['./writting.component.scss']
})
export class WrittingComponent implements OnInit {

  m: Message;
  constructor(
    private service: DataMessagesService
  ) { }

  ngOnInit(): void {
    this.m = {
      content : 'coucou',
    title : 'titre',
    sent : new Date(),
    isRead : false
    }
  }

  addNewMessage(){
    this.service.addMessage(this.m);
  }

}
