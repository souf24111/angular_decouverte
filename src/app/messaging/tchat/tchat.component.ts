import { InstantMessagingService } from './../instant-messaging.service';
import { Component, OnInit, EventEmitter } from '@angular/core';

export interface Message {
  author: User;
  content: string;
  datetime: Date;

}

export interface User {
  nick: string;
  sent?: Message[];
  received?: Message[];
  online?: boolean;
}


@Component({
  selector: 'app-tchat',
  templateUrl: './tchat.component.html',
  styleUrls: ['./tchat.component.scss']
})
export class TchatComponent implements OnInit {

  /**
   * ZONE de déclaration
   */
  user: User = {
    nick : 'Git stache !!!'
  };


  messages: Message[] = [
    {
      author : this.user,
      content : 'Premier message du tchat',
      datetime : new Date()
    },
    {
      author : this.user,
      content : 'Second message du tchat',
      datetime : new Date()
    },
    {
      author : this.user,
      content : 'Troisieme message du tchat',
      datetime : new Date()
    }
  ]

/**
 * ZONE d'initialisation
 */


  constructor(
    private service: InstantMessagingService
    ) {
 }

 /**
  * initialisation du component (subscibe aux observables ....)
  */
  ngOnInit(): void {

 }

 /**
  * ZONE d'implémentation
  */

 delete(message: Message){
   console.log(message);
   this.messages.splice(this.messages.indexOf(message),1);


 }

}
