import { Message } from './../tchat/tchat.component';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tchat-message',
  templateUrl: './tchat-message.component.html',
  styleUrls: ['./tchat-message.component.scss']
})
export class TchatMessageComponent implements OnInit {


  @Input('data') message: Message;

  @Output('remove') deleteEvent = new EventEmitter<Message>();

  constructor() { }

  ngOnInit(): void {
  }


  edit(): void {
    this.message.content = this.message.content.toUpperCase();
  }

  delete(): void {
    this.deleteEvent.emit(this.message);
  }
}
